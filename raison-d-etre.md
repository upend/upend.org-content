# UpEnd's raison d'être

*THIS IS A FIRST DRAFT ONLY*



## principles

- **People / Users / Society First.** Don't treat visitors of your site or users of your service as the product. Don't sell them out to advertisers. Simply don't sell them. Serve them, and serve them only. There is such a thing as [Moral Design](https://web.archive.org/web/20130402162847/http://fadeyev.net/2012/06/19/moral-design/).

- **Concentration of power is inherently dangerous.** The hegemony of big companies like Facebook, Google, Amazon over the internet is not just unhealthy because it undermines freedom. Power like that can only be built though dishonesty, manipulation and exploitation. All that power is not exercised in society's interests, but in their own, and in the interests of the advertisers who pay them huge sums.  One cannot attain such power and be moral at the same time. It simply isn't possible. 

- **Independent and conflict-of-interest-free systems** are as critical to a free and fair internet as they are to a free and fair judicial system, to a free and fair press. There is such as thing as "the commons", and the commons should not be privatized.

- **Advertising is the internet's original sin.** Advertising is a cancer on the internet. [Advertising is our C8](https://news.ycombinator.com/item?id=10047706).  That advertising gives you [free stuff is the biggest lie of the web](https://news.ycombinator.com/item?id=8585237). The web is riff with [perverse incentives](https://en.wikipedia.org/wiki/Perverse_incentive), and advertising is the greatest of them all. Shall I continue?

- **Honesty, character and ethics matter.** We continue to patronize businesses and websites that are dishonest or play us for fools (in which case, we are fools). Dishonesty must be held accountable. We must vote with our feet. It's the failure to do so that is behind the stubborn persistence of so many ills.  *We can’t wait for the "free" market to sort this out, because it is far too busy milking this mess for all it’s worth.* We are neither buyers nor sellers nor even bystanders in this market, we are the goods. *We must stop being the goods.*

- **Living our convictions** is critical. The key to making the world a better place is for each of us to live our convictions. The reality is, more often than not, we don't. We know we shouldn't reward Facebook for their moral failures and dishonesty. But we keep giving them more power anyway. We are complicit. We reap what we sow.

*Living our convictions is not easy, but they aren't convictions if we don't live them.*

*The truth of our convictions lie in the actual choices we make.*



## mission

- **Raise awareness** of the various problems that undermine a healthy and free internet (more frankly, that gave us the mostly shitty web we have).
- **Enlighten** people of the principles (above) necessary for a different web (and world).
- **Connect** people with options better aligned with the principles. Provide best practices, guides and other help to facilitate the adoption of more principled solutions by more people.
- **Promote** sites and services that adhere to the principles, or are at least a significant improvement. Where none exist, promote their development. 
- **Demote** sites and services that violate the principles. Call them out. Campaign against them. Provide users with help in finding alternatives. 
- **Propose** solution directions for critical areas like journalism and civic discourse, critical pieces of society that are under siege in today's internet. **Challenge** people and groups to fill the gaps.

In essence, to provide both **moral and practical support** to a nascent movement. 

The status quo is not acceptable. It needs to be #resisted, #subverted, and #UpEnded. 



## tone & discipline

A website that is a rant will get us nowhere.

We can make much more of a difference with a **concerted, disciplined voice** rooted in impassioned values but built up with facts and reason.  The more clear, succinct, rational and honest our argument and presentation, the more likely it will get heard.

That said, the occasional [quality rant](https://youtu.be/-hL4lMcIqS4) can be inspiring.

